const functions = require("firebase-functions");
const express = require('express');
const cors = require('cors');
const jwt = require('jsonwebtoken');


const admin = require('firebase-admin');
admin.initializeApp({ databaseURL: "https://reallift-502df-default-rtdb.firebaseio.com/" });
const firebaseRef = admin.database().ref('/');

const CLIENT_ID = "608247718211-fl4tbjbths0ppjfv5qk77fn622mndgvv.apps.googleusercontent.com";
const ACTION_CLIENT_ID = "747846313690-hig9ht1vunheeslij2rp6ibdheb1q2ko.apps.googleusercontent.com"

const CLIENT_SECRET = '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';

exports.auth = functions.https.onRequest(async (req, res) => {
    functions.logger.log("/auth");
    functions.logger.log(req.body);
});

exports.login = functions.https.onRequest((req, res) => {
    if (req.method === 'GET') {
        functions.logger.log('Requesting admin page');
        // functions.logger.log("Responseurl:" + request.query.responseurl);
        // functions.logger.log(JSON.stringify(request.query));
        // functions.logger.log(request.query);
        res.send(`
        <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <script src="https://apis.google.com/js/platform.js" async defer></script>
    
        <meta name="google-signin-client_id"
            content=${CLIENT_ID}>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>
    </head>
    
    <body>
    
        <h1>Login</h1>
        
    
        <form id="loginform" action="/admin" method="get">
    
            <input type="hidden"
            name="responseurl" value="" />
            <input type="hidden"
            name="token" value="" />
            
            </br></br></br>
            <div class="g-signin2" data-onsuccess="onSignIn"></div>
            <!--<a href="#" onclick="signOut();">Sign out</a>-->
        </form>
    
        <script>
            function onSignIn(googleUser) {
                var id_token = googleUser.getAuthResponse().id_token;
                const form = document.getElementById('loginform');
                form.elements.item(1).value = id_token;
                form.submit();
            }
    
            function signOut() {
                var auth2 = gapi.auth2.getAuthInstance();
                auth2.signOut().then(function () {
                    console.log('User signed out.');
                });
            }
    
            window.onbeforeunload = function(e){
              gapi.auth2.getAuthInstance().signOut();
            };
        </script>
    
    </body>
    
    </html>
      `);
    } else if (req.method === 'POST') {

    } else {
        // Unsupported method
        res.send(405, 'Method Not Allowed');
    }
});

exports.admin = functions.https.onRequest((req, res) => {
    functions.logger.log(req);
    //functions.logger.log(req.method);
    // if (req.method === 'GET') {
    functions.logger.log('Requesting admin page');
    functions.logger.log(req.query.token);
    var codedToken = decodeURIComponent(req.query.token);
    functions.logger.log(JSON.stringify(codedToken));
    var token = parseJwt(codedToken);

    var isAdminUser = false;

    if (token.sub == "102341818661566319909") {
        functions.logger.log('Its jake');
        isAdminUser = true
        
    } else {
        functions.logger.log('Regular user');
    }
    // functions.logger.log("Responseurl:" + request.query.responseurl);
    // functions.logger.log(JSON.stringify(request.query));
    // functions.logger.log(request.query);

    var userIDForm = "";
    if (isAdminUser){
        userIDForm = `<label for="userID">UserID:</label><br>
        <input type="text" id="userID" name="userID" value="` + token.sub + `"><br>`;
    } else {
        userIDForm = `<input type="hidden" id="userID" name="userID" value="` + token.sub + `"><br>`;
    }

    res.send(`
        <!DOCTYPE html>
    <html lang="en">
    
    <head>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>
    </head>
    
    <body>
    
        <h1>Link User to Device</h1>

        <form id="linkDevice" action="/linkDevice" method="post">
            ` + userIDForm + `
            <label for="deviceID">DeviceID:</label><br>
            <input type="text" id="deviceID" name="deviceID" value="00:00:00:00:00:00"><br><br>
            <input type="submit" value="Submit">
        </form> 
    
    </body>
    
    </html>
      `);
    // }
});

exports.linkDevice = functions.https.onRequest((req, res) => {
    functions.logger.log('/linkDevice');
    if (req.method === 'POST') {
        var userID = decodeURIComponent(req.body.userID);
        var deviceID = decodeURIComponent(req.body.deviceID);
        functions.logger.log('Linking user:' + userID + ' to device: ' + deviceID);
        AssignDeviceToUser(userID, deviceID);
        res.json({ message: 'success' });
    }
});


async function CreateUser(userID, userName, userEmail) {
    functions.logger.log("Creating user");
    functions.logger.log(userID);
    functions.logger.log(userName);
    functions.logger.log(userEmail);
    var user = {
        userName: userName,
        userEmail: userEmail,
        devices: null
    };
    const abc = await firebaseRef.child('users').child(userID).set(user);
}

async function AddTestDevices() {
    var device = {
        deviceID: "abc123",
        deviceType: "liftSplicer",
        targetFloor: 0,
        currentFloor: 0,
        targetDoorState: 'closed',
        currentDoorState: '',
        updateFirmware: false,
        currentFirmwareVersion: 0
    };

    var device2 = {
        deviceID: "afadfads",
        deviceType: "liftSplicer"
    };

    const cba = await firebaseRef.child('devices').child(device.deviceID).set(device);
    const abc = await firebaseRef.child('devices').child(device2.deviceID).set(device2);
}

async function AssignDeviceToUser(userID, deviceID) {
    const cba = await firebaseRef.child('users').child(userID).child('devices').child(deviceID).set({ device: deviceID });
}

exports.token = functions.https.onRequest(async (req, res) => {
    functions.logger.log("/token");
    var incomingToken = parseJwt(req.body.assertion);


    var newRecord = { token: incomingToken };
    //const abc = await firebaseRef.set(newRecord);


    if (incomingToken.iss != "https://accounts.google.com") {
        functions.logger.error("iss != https://accounts.google.com");
        res.status(200).json({ message: "incoming token error" });
    } else {
        functions.logger.log("iss == https://accounts.google.com");
    }

    //AddTestDevices();

    // Create user record
    var user = {
        userName: incomingToken.given_name,
        userEmail: incomingToken.email,
        devices: null
    };

    await firebaseRef.child('users').child(incomingToken.sub).set(user);
    await firebaseRef.child('users').child(incomingToken.sub).child('devices').set({});



    functions.logger.log(JSON.stringify(incomingToken));
    const secondsInDay = 86400; // 60 * 60 * 24
    const access = jwt.sign({
        sub: incomingToken.sub,
        aud: CLIENT_ID
    }, CLIENT_SECRET, {
        expiresIn: '1h'
    });
    functions.logger.log("2");
    const refresh = jwt.sign({
        sub: incomingToken.sub,
        aud: CLIENT_ID
    }, CLIENT_SECRET);

    // Return auth token to google
    obj = {
        token_type: 'bearer',
        access_token: access,
        refresh_token: refresh,
        expires_in: secondsInDay,
    };

    res.status(200)
        .json(obj);
});

exports.voice = functions.https.onRequest(async (req, res) => {
    functions.logger.log('Received a request on /voice');
    functions.logger.log(JSON.stringify(req.body));
    functions.logger.log(JSON.stringify(req.headers.authorization));
    // Grab the text parameter.
    //const original = req.query.text;    



    // Confirm user exists and get record from DB
    var token;
    if (req.headers.authorization.includes("Bearer")) {
        token = req.headers.authorization.split('Bearer ')[1];
    } else {
        token = req.headers.authorization;
    }

    functions.logger.log(token);
    // Use UID to get name and devices
    decoded = parseJwt(token);


    if (req.body.handler.name == 'auth') {
        authorizeUser(req, res, decoded);
        return;
    }

    var userID = decoded.sub;
    var userRecord = await firebaseRef.child('users').child(userID).once('value');
    functions.logger.log("Got user record");
    functions.logger.log(JSON.stringify(userRecord));
    userRecord = JSON.parse(JSON.stringify(userRecord)); // convert from datasnapshot to object

    if (JSON.stringify(userRecord) != "null") {
        functions.logger.log("record exists: " + userID);
    } else {
        res.json({ message: "Error: User not in RealLift database" });
    }






    if (req.body.handler.name == 'press_one') {
        var deviceID = Object.keys(userRecord.devices)[0]; // gets first device (this isn't the best)
        var response = await press_one(req, deviceID);
        res.json(response);

    } else if (req.body.handler.name == 'press_two') {
        var deviceID = Object.keys(userRecord.devices)[0]; // gets first device (this isn't the best)
        var response = await press_two(req, deviceID);
        res.json(response);

    } else if (req.body.handler.name == 'press_three') {
        var deviceID = Object.keys(userRecord.devices)[0]; // gets first device (this isn't the best)
        var response = await press_three(req, deviceID);
        res.json(response);

    } else if (req.body.handler.name == 'registerdevice') {
        res.json(registerdevice(req, userID));

    } else if (req.body.handler.name == 'try_update') {
        var deviceID = Object.keys(userRecord.devices)[0]; // gets first device (this isn't the best)
        var response = await try_update(req, deviceID);
        res.json(response);
    }
});

exports.greeting = functions.https.onRequest(async (req, res) => {
    // Grab the text parameter.
    //const original = req.query.text;
    functions.logger.log(req);

    functions.logger.log('Received a request on /greeting');
    res.json({ result: 'Received a request on /greeting' });
});

async function authorizeUser(req, res, userToken) {
    functions.logger.log('auth');
    functions.logger.log(JSON.stringify(userToken));


    const test = await firebaseRef.child('users').child(userToken.sub).once('value');
    if (JSON.stringify(test) != "null") {
        functions.logger.log("record exists");
    } else {
        functions.logger.log("record doesn't exist, creating");
        CreateUser(userToken.sub, userToken.given_name, userToken.email);
    }

    var userName = userToken.given_name;
    var response = {};


    response.session = req.body.session;
    response.prompt = {
        override: false,
        firstSimple: {
            speech: "Hi " + userName + ", ready for commands.",
            text: "Hi " + userName + ", ready for commands.\n"
        }
    };
    response.scene = {
        name: "actions.scene.START_CONVERSATION",
        slotFillingStatus: "UNSPECIFIED",
        slots: {},
        next: {
            name: "Ready"
        }
    };

    functions.logger.log(req);

    functions.logger.log(JSON.stringify(response));
    res.json(response);
}

function registerdevice(req, userID) {
    functions.logger.log("/registerdevice");
    //functions.logger.log(JSON.stringify(req));


    response = {};
    response.session = req.body.session;
    response.prompt = {
        override: false,
        firstSimple: {
            speech: "Device registered",
            text: "Device registered\n"
        }
    };
    response.scene = {
        name: "actions.scene.Ready",
        slotFillingStatus: "UNSPECIFIED",
        slots: {},
        next: {
            name: "Ready"
        }
    };

    AssignDeviceToUser(userID, "4C:75:25:5C:EF:74");

    functions.logger.log(JSON.stringify(response));

    return (response);

}

async function press_one(req, deviceID) {
    functions.logger.log("/press_one");
    //functions.logger.log(JSON.stringify(req));


    response = {};
    response.session = req.body.session;
    response.prompt = {
        override: false,
        firstSimple: {
            speech: "Pressing Button One",
            text: "Pressing Button One\n"
        }
    };
    response.scene = {
        name: "actions.scene.Ready",
        slotFillingStatus: "UNSPECIFIED",
        slots: {},
        next: {
            name: "Ready"
        }
    };

    await firebaseRef.child('devices').child(deviceID).child('targetFloor').set(0);

    functions.logger.log(JSON.stringify(response));
    return (response);
}

async function press_two(req, deviceID) {
    functions.logger.log("/press_two");
    //functions.logger.log(JSON.stringify(req));

    response = {};
    response.session = req.body.session;
    response.prompt = {
        override: false,
        firstSimple: {
            speech: "Pressing Button Two",
            text: "Pressing Button Two\n"
        }
    };
    response.scene = {
        name: "actions.scene.Ready",
        slotFillingStatus: "UNSPECIFIED",
        slots: {},
        next: {
            name: "Ready"
        }
    };

    await firebaseRef.child('devices').child(deviceID).child('targetFloor').set(1);
    functions.logger.log(JSON.stringify(response));
    return (response);
}


async function press_three(req, deviceID) {
    functions.logger.log("/press_three");
    //functions.logger.log(JSON.stringify(req));

    response = {};
    response.session = req.body.session;
    response.prompt = {
        override: false,
        firstSimple: {
            speech: "Pressing Button Three",
            text: "Pressing Button Three\n"
        }
    };
    response.scene = {
        name: "actions.scene.Ready",
        slotFillingStatus: "UNSPECIFIED",
        slots: {},
        next: {
            name: "Ready"
        }
    };

    await firebaseRef.child('devices').child(deviceID).child('targetFloor').set(2);
    functions.logger.log(JSON.stringify(response));
    return (response);
}


async function try_update(req, deviceID) {
    functions.logger.log("/try_update");
    //functions.logger.log(JSON.stringify(req));

    response = {};
    response.session = req.body.session;
    response.prompt = {
        override: false,
        firstSimple: {
            speech: "Checking for firmware updates",
            text: "Checking for firmware updates\n"
        }
    };
    response.scene = {
        name: "actions.scene.Ready",
        slotFillingStatus: "UNSPECIFIED",
        slots: {},
        next: {
            name: "Ready"
        }
    };

    await firebaseRef.child('devices').child(deviceID).child('tryUpdate').set(1);
    functions.logger.log(JSON.stringify(response));
    return (response);
}

function parseJwt(token) {
    var base64Payload = token.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    return JSON.parse(payload.toString());
}



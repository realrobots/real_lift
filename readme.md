## Instructions

#### CONNECT BOARD TO WIFI AND UPDATE FIRMWARE

Connect to splicer device using red USB-UART board, match the GND pin on the board with the GND pin on the red board.

Connect to computer and talk to it using the Arduino Serial Monitor at baud rate 115200

It might not have enough current from the USB-UART board, if that's the case it'll be stuck in a reboot cycle and you'll have to power it with 5v to the screw terminal

Using the serial monitor you need to give it your wifi credentials, you can type '?' to get instructions
Type the following to connect to your wifi and download the latest firmware

0=YOURWIFINAME

1=YOURWIFIPASSWORD

r

9

'r' reboots it so it'll connect to the credentials you gave it, '9' tells it to check for updates. If it finds the update it'll give a huge list of numbers as it updates.

That's all you need to do on the device, when it reboots it'll be connected to my cloud server, waiting for commands.

When the device resets it'll print a MAC address to the console (eg. 32:F2:91:00:2B:9D). copy and paste that into a notepad because you'll need it soon.



#### CONNECT GOOGLE ASSISTANT AND YOUR GOOGLE ACCOUNT TO MY CLOUD SERVER

I'm not approved by google yet so I need to manually add you to my list of testers, then if you go to 
https://console.actions.google.com/

login to your google account, choose "RealLift", press the "TEST" tab and try the "Talk to Lift" command.

The first thing the test environment will do is ask you to link your google account, say yes, then yes again. After that you should be able to use it from here or directly on your phone.

You just need to connect a device to your account now go to the link below

https://us-central1-reallift-502df.cloudfunctions.net/login

Press the sign in button and give it your same google account.

You should get a page that wants a deviceID, paste the MAC address from earlier in there and press Submit.

That's it, now you should be able to talk to the google assistant and say, "talk to lift" to start listening to commands.

You can say "press button one", "press button two", and "press button three" and your splicer board should respond, the indicator light on it should flash while it presses any button.

You can also download the latest firmware update by using the command "update".